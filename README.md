# Documentation Projet réseau

Voici une petite documentation qui va vous expliquer les étapes à suivre afin de mettre votre serveur web en place. <br/> <br/>

**Prérequis** <br/>

- Une machine virtuel linux avec l'os de votre choix (débian 11 pour notre cas)
- Config de la machine : <br/>
2go CPU <br/>
16go de stockage <br/> <br/>

## I./Serveur Web
Tout d’abord nous avons besoins de choisir un serveur web qui nous permettra d’héberger notre site. <br/>

Dans notre cas nous allons utiliser un VPS du site OVH. (Libre à vous de choisir l’hébergeur qui vous convient). <br/>

Une fois le serveur acheté et les identifiants récupéré nous allons pouvoir passer à la suite <br/>

### Mise en place du serveur

Nous pouvons enfin rentrer dans le vif du sujet. <br/>

Lançons notre VM et commençons la configuration. Si vous n'avez aucun terminal d'ouvert, ouvrez en un. <br/>

Et entré la commande suivant : <br/>

```apt install openssh-client``` <br/> 

Cela va installer le paquet OpenSSH. <br/> 

Ensuite nous allons modifier le port d'écoute qui de base est sur 22. <br/> 
Modifier le port applique une sécurité contre les attaques de bots, ce qui va nous permettre de ne pas être dans le circuit d'envoies de requetes en masse des bots. <br/> <br/>

Pour ce faire nous allons écrire :<br/> 

```nano /etc/ssh/sshd_config``` <br/>

Une fois dans le fichier il faut chercher les lignes suivantes : <br/>
``` What ports, IPs and protocols we listen for``` <br/>
``` Port 22 ``` <br/>

On remplace le '22' par le numéro de notre choix, pour nous ça sera le 50123.<br/>

On peut désormais enregistrer les modifications et quitter le fichier.<br/>

Puis on redémarre le service : <br/>
```systemctl restart sshd``` <br/><br/>

Notez que quand vous voudrez vous connecter à votre serveur en SSH il faudra indiquer votre nouveau port : <br/>
```username@IPv4_of_your_VPS -p NewPortNumber```<br/> <br/>


**Utilisateur Root** <br/>

On va maintenant désactiver l'accès au serveur via l'utilisateur root, comme ça si un méchant hacker arrive à avoir les log du User Root il ne pourra pas accéder à notre serveur en SSH. <br/>

Avant cela pensez à ajouter un nouveau user : <br/>
```adduser NomUtilisateurPersonnalisé```<br/>

Remplissez les informations demandé. <br/> <br/>

Revenons à notre utilisateur root.<br/>
Pour modifier le fichier de configuration SSH : <br/>
```nano /etc/ssh/sshd_config``` <br/><br/>

Cherchez la section : <br/>
```# Authentification :``` <br/>
```LoginGraceTime 120``` <br/> 
```PermitRootLogin yes``` <br/>
```StrictModes yes``` <br/>

Remplacez ```yes``` par ```no``` sur la ligne ```PermitRootLogin```. <br/><br/>

Et on redémarre le service ssh pour que nos modification soit prise en compte : <br/>
```systemctl restart sshd``` <br/><br/>
Voila, maintenant les connexions ssh via l'utilisateur root sont désormais rejetées. <br/> <br/>

## II./ Installation de fail2ban

A partir de là nous allons nous connecter à notre serveur via SSH. : <br/>
```username@IPv4_of_your_VPS -p PortNumber``` <br/>

Une fois cela fait, nous allons configurer Fail2ban <br/> <br/>

**Fail2ban c'est quoi ?** <br/>
Fail2ban est un framework de prévention contre les intrusions dont le but est de bloquer les adresses IP inconnues qui tentent de pénétrer dans votre système.<br/> <br/>

Dans un premier temps on va installer le package : <br/>
```apt-get install fail2ban``` <br/> <br/>

Une fois le paquet installé, il faut modifier le fichier de configuration de ce dernier pour l’adapter à votre usage. Avant toute modification, il est recommandé d’effectuer une sauvegarde du fichier de configuration en tapant la commande suivante : <br/>
```cp /etc/fail2ban/jail.conf /etc/fail2ban/jail.conf.backup``` <br/> <br/>

Maintenant on peut modifier le fichier comme on le souhaite : <br/>
```nano /etc/fail2ban/jail.conf``` <br/> <br/>

Ce qui va nous intéresser ici c'est la partie __Default__. <br/><br/>
Dans cette partie nous allons chercher ```ignoreip``` afin de lister les ip que l'on veut whitelister (les ip que l'on autorise, ces ip ne prendont donc pas en compte les paramètres ci-dessous),<br/><br/>
 ```bantime``` qui va nous permettre de définir le temps de ban des personnes qui essaient de s'introduire
 <br/> <br/>et enfin ```maxretry``` qui est le nombre d'essaie qu'un utilisateur peut faire avant de se faire ban. <br/> <br/>

Une fois les modification faites à votre guise, on quitte le fichier, puis on redémarre le service pour que les modifications soient prises en compte : <br/> 
```/etc/init.d/fail2ban restart``` <br/><br/>



**Voila notre serveur web est désormais sécurisé au minimum et prêt à l'utilisation. :)** <br/>

**En espérent que cette documentation vous aura été claire et précise. ;)**